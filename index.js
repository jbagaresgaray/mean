'use strict';

var env = process.env.NODE_ENV || 'development',
    application = require('./config/application'),
    express = require('express'),
    expressValidator = require('express-validator'),
    customValidator = require('./app/validator/')(expressValidator),
    morgan = require('morgan'),
    bodyParser = require('body-parser'),
    cookieParser = require('cookie-parser'),
    methodOverride = require('method-override'),
    session = require('express-session'),
    oauth = require('./config/oauth.js'),
    passport = require('passport'),
    LocalStrategy = require("passport-local").Strategy,
    FacebookStrategy = require('passport-facebook').Strategy,
    TwitterStrategy = require('passport-twitter').Strategy,
    GoogleStrategy = require('passport-google-oauth').OAuth2Strategy,
    bunyan = require('bunyan'),
    mongoose = require('mongoose'),
    config = require('./config/environment/' + env),
    middleware = require('./app/utils/middleware'),
    Database = require('./app/utils/database').Database,
    database = new Database(mongoose, config),
    nodemailer = require('nodemailer'),
    multipart = require('connect-multiparty'),
    log = bunyan.createLogger({
        name: config.app_name
    }),
    app = express();

app.use('/public', express.static(__dirname + '/public'));
app.set('port', process.env.APP_PORT || 3000);
app.set('api_version', process.env.APP_VER || '/api/v1');
app.set('view engine', 'ejs');
app.set('views', 'app/view/');
app.use(morgan('dev'));
app.use(methodOverride());
app.use(cookieParser());
app.use(bodyParser.json({
    type: 'application/json',
    limit: '50mb'
}));
app.use(bodyParser.urlencoded({
    extended: true,
    limit: '50mb'
}));
app.use(session({
    secret: 'mindanaojobs123',
    resave: true,
    saveUninitialized: true
}));
app.use(passport.initialize());
app.use(passport.session());
app.use(middleware.allowCrossDomain);
app.use(expressValidator());
app.use(multipart({
    uploadDir: config.tmp
}));

require(application.utils + 'helper')(database, app, log);
require(application.utils + 'loadschema')(mongoose);
require(application.controller + 'jobCategoriesController')(mongoose);
require(application.controller + 'locationWebController')(mongoose);
require(application.controller + 'skillsWebController')(mongoose);
require(application.routes + 'email')(app);
require(application.routes)(app,passport);
// require(application.routes + 'api')(app);
// require(application.routes + 'session')(app, opentok, mongoose);
require(application.routes + 'login')(app, passport, mongoose,oauth,LocalStrategy, FacebookStrategy,TwitterStrategy,GoogleStrategy, middleware.isLoggedIn);

module.exports = app;
