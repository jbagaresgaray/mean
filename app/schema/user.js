'use strict';

module.exports = function(mongoose) {
    var UserSchema = new mongoose.Schema({
        email: String,
        password: String,
        FirstName: String,
        LastName: String,
        age: Number,
        gender: String,
        address: String,
        city: String,
        StateProvince: String,
        location:String,
        desiredSalary: Number,
        photo: String,
        height: String,
        qualification: String,
        confirm: Boolean,
        birthdate: Date,
        profession: String,
        facebook: {
            id: String,
            token: String,
            email: String,
            name: String
        },
        twitter: {
            id: String,
            token: String,
            displayName: String,
            username: String
        },
        google: {
            id: String,
            token: String,
            email: String,
            name: String
        },
        contacts: [{
            id: String,
            contact_number: String,
            contact_type: String
        }],
        skills: [{
            id: String,
            skill_name: String,
            proficiency: String
        }],
        specialization: [{
            id: String,
            specialization: String
        }],
        education: {
            elementary: {
                schoolname: String,
                year: Number
            },
            secondary: {
                schoolname: String,
                year: Number
            },
            under_grad: {
                schoolname: String,
                year: Number,
                course: String
            },
            post_grad: {
                schoolname: String,
                year: Number,
                course: String
            },
        },
        training: [{
            id: String,
            name: String,
            date: Date,
            trainor: String
        }],
        workExperienced: [{
            id: String,
            companyName: String,
            salary: Number,
            duration: {
                fromMonth: String,
                fromYear: Number,
                toMonth: String,
                toYear: Number
            },
            currentlyWork:Boolean,
            responsibility: String,
            location: String,
            description: String,
        }],
        Jobs: [{
            id: String,
            job_id: String,
            Status: Number
        }]
    });



    mongoose.model('Users', UserSchema);
};
