/*var textSearch = require('mongoose-text-search');

module.exports = function(mongoose) {
    var SkillsSchema = new mongoose.Schema({
        skills_name: String,
        value: String
    });

    SkillsSchema.plugin(textSearch);
    SkillsSchema.index({ skills_name: 'text',value: 'text' });
    mongoose.model('Skills', SkillsSchema);

};
*/

module.exports = function(mongoose) {
    var SkillsSchema = new mongoose.Schema({
        skills_name: String,
        value: String
    });
    SkillsSchema.index({ skills_name: 'text',value: 'text' });
    mongoose.model('Skills', SkillsSchema);

};
