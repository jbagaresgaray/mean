module.exports = function(mongoose) {
    var JobsSchema = new mongoose.Schema({
        description: String,
        categories: String,
        position: String,
        qualification: [{
            qualify: String
        }],
        requirements: [{
            require: String
        }],
        skills:[{
            name: String
        }],
        emp_id: {
            type: String,
            ref: 'Employer'
        },
        id: String,
        salary: String,
        YrsExp: Number,
        employmentbasis: String,
        datepost: Date
    });


    mongoose.model('Jobs', JobsSchema);
};


