'use strict';

module.exports = function(mongoose) {
    var EmployerSchema = new mongoose.Schema({
        name: String,
        overview: String,
        address: String,
        contacts: [{
            number: String,
            NumberType: String
        }],
        website: String,
        email: String,
        password: String,
        image: String
    });

    mongoose.model('Employer', EmployerSchema);
};
