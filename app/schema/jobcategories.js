module.exports = function(mongoose) {
    var JobsCategories = new mongoose.Schema({
        description: String,
        value: String,
        subcategory:[{
          description: String,
          value:String
        }],
        id: String
    });

    mongoose.model('JobsCategories', JobsCategories);
};
