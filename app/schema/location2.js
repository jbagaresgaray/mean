module.exports = function(mongoose) {
    var Location2Schema = new mongoose.Schema({
        region: String,
        regionalcenter: String,
        Town_Cities:[{
            locationname:String
        }],
        id: String
    });

    mongoose.model('location2', Location2Schema);
};
