module.exports = function(mongoose) {
    var LocationSchema = new mongoose.Schema({
        town_cities_name: String,
        id: String
    });

    mongoose.model('location', LocationSchema);
};
