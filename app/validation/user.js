exports.validateUser = function(req, res, next) {

    req.assert('email', 'Please enter an Email Address').notEmpty();
    req.assert('email', 'Email address needs to be in the format yourname@domain.com.').isEmail();
    req.assert('password', '6 to 10 characters required').len(6, 10);
    req.assert('password', 'Please enter a Password').notEmpty();
    req.assert('FirstName', 'Please enter your First Name').notEmpty();
    req.assert('LastName', 'Please enter your Last Name').notEmpty();
    req.assert('gender', 'Gender is required').notEmpty();
    req.assert('address', 'Address is required').notEmpty();
    req.assert('desiredSalary', 'Desired salary is required').notEmpty();
//    req.assert('photo', 'Photo is required').notEmpty();
    req.assert('height', 'Height is required').notEmpty();
    req.assert('qualification', 'Qualification Info is required').notEmpty();

    var errors = req.validationErrors();

    if (errors) {
        res.status(200).send(errors);
    } else {
        next();
    }
};

exports.validateID = function(req, res, next) {

    req.checkParams('id', 'Invalid "id" URL Parameter').isAlpha();

    var errors = req.validationErrors();
    if (errors) {
        res.status(200).send(errors);
    } else {
        next();
    }
};

exports.validateSignUp = function(req, res, next) {

    req.assert('FirstName', 'Please enter your First Name').notEmpty();
    req.assert('LastName', 'Please enter your Last Name').notEmpty();
    req.assert('email', 'Please enter an Email Address').notEmpty();
    req.assert('email', 'Email address needs to be in the format yourname@domain.com.').isEmail();
    req.assert('password', '6 to 10 characters required').len(6, 10);
    req.assert('password', 'Please enter a Password').notEmpty();
    req.assert('confirmpassword', 'Please confirm your Password').notEmpty();
    req.assert('confirmpassword', 'Passwords do not match').equals(req.body.password);


    var errors = req.validationErrors();

    if (errors) {
        res.status(200).send(errors);
    } else {
        next();
    }
}

