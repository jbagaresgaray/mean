'use strict';

exports.validateJobs = function(req, res, next) {

    // req.assert('description', 'Job Description is required').notEmpty();
    // req.assert('position', 'Job Position is required').notEmpty();
    // req.assert('address', 'Employer Address is required').notEmpty();
    // req.assert('empID', 'Employer Details is required').notEmpty();
    // req.assert('salary', 'Invalid Job Salary format').isInt();
    // req.assert('salary', 'Job Salary is required').notEmpty();

    /*    req.assert('qualification', 'Job Qualification is not an Array').isArray();
    req.assert('requirements', 'Job Requirements is not an Array').isArray();
    req.assert('contact', 'Job Contact Details is not an Array').isArray();
    req.assert('email', 'Job Contact Email is not an Array').isArray();
*/

    var errors = req.validationErrors();
    if (errors) {
        res.status(200).send(errors);
    } else {
        next();
    }
};
