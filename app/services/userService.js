'use strict';


var userDao = require('../daos/userDao');

var User = function() {
    var self = this;

    self.saveUser = function saveUser(data, next) {
        userDao.saveUser(data, next);
    };

    self.updateUser = function saveUser(value,data, next) {
        userDao.updateUser(value,data, next);
    };

    self.countUser = function countUser(id, next) {
        userDao.countUser(next);
    };

    self.getAllUser = function getAllUser(next) {
        userDao.getAllUser(next);
    };

    self.findUser = function findUser(id,next){
        userDao.findUser(id,next);
    };

    self.deleteUserByID = function deleteUserByID(id,next){
        userDao.deleteUserByID(id,next);
    };

    self.findUserByValue = function findUserByValue(value,next){
        userDao.findUserByValue(value,next);
    }

    self.recoverUserPassword = function recoverUserPassword(id,next){
        userDao.recoverUserPassword(id,next);
    };

    self.findUserBySocial = function findUserBySocial(type,id,next){
        userDao.findUserBySocial(type,id,next);
    }

    self.saveUserBySocial = function saveUserBySocial(type,token,data,next){
        userDao.saveUserBySocial(type,token,data,next);
    }

};


exports.User = User;
