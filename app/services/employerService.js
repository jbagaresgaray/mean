'use strict';

var employerDao = require('../daos/employerDao');

var employer = function(){
	var me = this;

	//Create
	me.CreateEmployer = function CreateEmployer(data, next){
		console.log('Services',data);
		employerDao.CreateEmployer(data, next);
	}

	//Read
	me.ReadEmployer = function ReadEmployer(data, next){
		employerDao.ReadEmployer(data, next);
	}

	me.ReadEmployerByid = function ReadEmployerByid(data, next){
		employerDao.ReadEmployerByid(data, next);
	}

	//Update
	me.UpdateEmployer = function UpdateEmployer(id, data , next){
		employerDao.UpdateEmployer(id, data, next);
	}

	//Delete
	me.DeleteEmployer = function DeleteEmployer(data, next){
		employerDao.DeleteEmployer(data, next);
	}
};

exports.employer = employer;
