'use strict';


var prepDao = require('../daos/prepDaos');

var Prep = function() {
    var self = this;

    self.getAllJobLocation = function getAllJobLocation(next) {
        prepDao.getAllJobLocation(next);
    };

    self.getAllJobLocation2 = function getAllJobLocation2(next) {
        prepDao.getAllJobLocation2(next);
    };

    self.getAllJobCategories = function getAllJobCategories(next) {
        prepDao.getAllJobCategories(next);
    };

    self.getAllSkills = function getAllSkills(next) {
        prepDao.getAllSkills(next);
    };

    self.findSkills = function findSkills(value,next){
        prepDao.findSkills(value,next);
    }


};


exports.Prep = Prep;
