'use strict';
var jobDao = require('../daos/jobDao');

var Jobs = function() {
    var self = this;

    self.saveJobs = function saveUser(data, next) {
        jobDao.saveJobs(data, next);
    };

    self.getAllJob = function getAllJob(data, next) {
        jobDao.getAllJob(data,next);
    };

    self.getCompanyProfWithJob = function getCompanyProfWithJob(data, next){
        jobDao.getCompanyProfWithJob(data, next);
    };

    self.showJobDetails = function showJobDetails(_jobid, next) {
        jobDao.showJobDetails(_jobid, next);
    };

    self.getjobEmpBase = function getjobEmpBase(data, next){
        jobDao.getjobEmpBase(data, next);
    };

    self.updateJob = function updateJob(_id, data, next) {
        jobDao.updateJob(_id, data, next);
    };

    self.findJobById = function findJobById(id, next) {
        jobDao.findJobById(id, next);
    };

    self.findOneJob = function findOneJob(value, next) {
        jobDao.findOneJob(value, next);
    };

    self.deleteJob = function deleteJob(id, next) {
        jobDao.deleteJob(id, next);
    };
};

exports.Jobs = Jobs;
