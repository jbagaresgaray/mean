'use strict';

var mongoose = require('mongoose');
var Job = mongoose.model('Jobs');

// Save data
exports.saveJobs = function saveJobs(data, next) {
    Job.create(data, next);
};

exports.findJob = function findJob(id, next) {
    Job.findById(id, next);
};

exports.updateJob = function updateJob(id, data, next) {
    Job.update(id, data, next);
}

exports.getAllJob = function getAllJob(next) {
	Job
		.find(next)
		.populate('emp_id')
		.exec(function (err, data) {
		  if (err) return handleError(err);
		})
};

exports.getjobEmpBase = function getjobEmpBase(data, next) {
    Job
		.find({emp_id: data},next)
		.populate('emp_id')
		.exec(function (err, data) {
		  if (err) return handleError(err);
		})
}

exports.showJobDetails = function showJobDetails(_jobid, next) {
   	Job
   		.find({_id: _jobid},next)
   		.populate('emp_id')
   		.exec(function(err, data){
   			if(err) return handleError(err);
   		});
}

exports.getCompanyProfWithJob = function getCompanyProfWithJob(emp_id, next){
	Job
		.find({emp_id: emp_id}, next)
		.populate('emp_id')
		.exec(function(err, data){
			if(err) return handleError(err);
		})
}

/*exports.countUser = function countUser(next) {
    User.find().count(next);
};*/
//
