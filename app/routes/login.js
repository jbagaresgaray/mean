'use strict';

module.exports = function(app, passport, mongoose, oauth, LocalStrategy, FacebookStrategy,TwitterStrategy,GoogleStrategy, isLoggedIn) {

    var User = mongoose.model('Users');
    var Employer = mongoose.model('Employer');

    var userService = require('../services/userService').User;
    var UserSrv = new userService();


    //  ===============================================================================================================
    //                                                  L O C A L
    //  ===============================================================================================================

    passport.use('user', new LocalStrategy(
        function(username, password, done) {
            User.findOne({
                email: username
            }, function(err, user) {
                if (err) {
                    return done(err);
                }
                if (!user) {
                    console.log('Hello no user');
                    return done(null, false, {
                        message: 'Incorrect username.'
                    });
                }

                if (user.password != password) {
                    console.log('Hello no password');
                    return done(null, false, {
                        message: 'wrong password.'
                    });
                }
                return done(null, user);
                console.log('Hello correct');
            });
        }
    ));

    passport.use('employer', new LocalStrategy(
        function(username, password, done) {
            console.log('account', username, password);
            Employer.findOne({
                email: username
            }, function(err, user) {
                if (err) {
                    return done(err);
                }
                if (!user) {
                    console.log('Hello no user');
                    return done(null, false, {
                        message: 'Incorrect Empusername.'
                    });
                }

                if (user.password != password) {
                    console.log('Hello no password');
                    return done(null, false, {
                        message: 'Incorrect Emppassword.'
                    });
                }
                return done(null, user);
                console.log('Hello correct');
            });
        }
    ));

    // Passport session setup.
    //   To support persistent login sessions, Passport needs to be able to
    //   serialize users into and deserialize users out of the session.  Typically,
    //   this will be as simple as storing the user ID when serializing, and finding
    //   the user by ID when deserializing.  However, since this example does not
    //   have a database of user records, the complete Facebook profile is serialized
    //   and deserialized.
    passport.serializeUser(function(user, done) {
        done(null, user);
    });

    passport.deserializeUser(function(user, done) {
        done(null, user);
    });

    app.route('/loginFailure') .get(function handleRequest(req, res, next){
  		res.send(req.user);
  	});

  	app.route('/loginSuccess') .get(isLoggedIn, function handleRequest(req, res, next){
  		res.send(req.user);
  	});


  //  ===============================================================================================================
  //                                                  F A C E B O O K
  //  ===============================================================================================================


    app.route('/auth/facebook')
        .get(passport.authenticate('facebook', {
            scope: ['email', 'public_profile']
        }), function handleRequest(req, res, next) {
            console.log('auth facebook . . .');
        });

    app.route('/auth/facebook/callback').get(passport.authenticate('facebook',  {
			successRedirect: '/loginSuccess',
			failureRedirect: '/loginFailure'
		}),
        function handleRequest(req, res, next) {
            console.log('facebook callback . . . ');
        }
    );

    passport.use(new FacebookStrategy({
            clientID: oauth.facebook.clientID,
            clientSecret: oauth.facebook.clientSecret,
            callbackURL: oauth.facebook.callbackURL,
            passReqToCallback: true // allows us to pass in the req from our route (lets us check if a user is logged in or not)
        },
        function(req, token, refreshToken, profile, done) {

            process.nextTick(function() {
                console.log(req.user);
                if (!req.user) {

                    UserSrv.findUserBySocial('facebook', profile.id, function(err, user) {

                        if (err) {
                            return done(err);
                        }

                        // if the user is found, then log them in
                        if (user) {
                            console.log('user found');
                            return done(null, user.id); // user found, return that user
                        } else {
                            console.log('saving user');
                            UserSrv.saveUserBySocial('facebook', token, profile, function(err, newUser) {

                                if (err) {
                                    throw err;
                                }

                                // if successful, return the new user
                                return done(null, newUser.id);
                            });
                        }

                    });

                } else {

                    // user already exists and is logged in, we have to link accounts
                    var user = req.user; // pull the user out of the session

                    UserSrv.saveUserBySocial('facebook', token, profile, function(err, user) {

                        if (err) {
                            throw err;
                        }
                        console.log('update fb user info .............');
                        return done(null, user.id);

                    });

                }
            });
        }
    ));


  //  ===============================================================================================================
  //                                                  T W I T T E R
  //  ===============================================================================================================


    app.get('/auth/twitter', passport.authenticate('twitter'));

    app.route('/auth/twitter/callback').get(passport.authenticate('twitter', {
          successRedirect: '/loginSuccess',
          failureRedirect: '/loginFailure'
        }),
        function handleRequest(req, res, next) {
            console.log('twitter callback . . . ');
        }
    );

    passport.use(new TwitterStrategy({
            consumerKey: oauth.twitter.consumerKey,
            consumerSecret: oauth.twitter.consumerSecret,
            callbackURL: oauth.twitter.callbackURL,
            passReqToCallback: true // allows us to pass in the req from our route (lets us check if a user is logged in or not)
        },
        function(req, token, tokenSecret, profile, done) {
            process.nextTick(function() {
                console.log(req.user);
                if (!req.user) {
                    UserSrv.findUserBySocial('twitter', profile.id, function(err, user) {
                        if (err) {
                            return done(err);
                        }

                        // if the user is found, then log them in
                        if (user) {
                            console.log('user found');
                            return done(null, user.id); // user found, return that user
                        } else {
                            console.log('saving user');
                            UserSrv.saveUserBySocial('twitter', token, profile, function(err, newUser) {

                                if (err) {
                                    throw err;
                                }

                                // if successful, return the new user
                                return done(null, newUser.id);
                            });
                        }

                    });

                } else {

                    // user already exists and is logged in, we have to link accounts
                    var user = req.user; // pull the user out of the session

                    UserSrv.saveUserBySocial('twitter', token, profile, function(err, user) {

                        if (err) {
                            throw err;
                        }
                        console.log('update twitter user info .............');
                        return done(null, user.id);

                    });

                }
            });
        }
    ));


    //  ===============================================================================================================
    //                                                  G O O G L E P L U S
    //  ===============================================================================================================

    app.route('/auth/google').get(passport.authenticate('google',{ scope : ['https://www.googleapis.com/auth/userinfo.profile',
                                            'https://www.googleapis.com/auth/userinfo.email'] }));

    app.route('/auth/google/callback').get(passport.authenticate('google', {
          successRedirect: '/loginSuccess',
          failureRedirect: '/loginFailure'
        }),
        function handleRequest(req, res, next) {
            console.log('google callback . . . ');
        }
    );

    passport.use(new GoogleStrategy({
            clientID: oauth.googleOAuth2.clientID,
            clientSecret: oauth.googleOAuth2.clientSecret,
            callbackURL: oauth.googleOAuth2.callbackURL,
            passReqToCallback: true // allows us to pass in the req from our route (lets us check if a user is logged in or not)
        },
        function(req, accessToken,refreshToken, profile, done) {
            process.nextTick(function() {
                console.log(req.user);
                if (!req.user) {
                    UserSrv.findUserBySocial('google',profile.id, function(err, user) {
                        if (err) {
                            return done(err);
                        }

                        // if the user is found, then log them in
                        if (user) {
                            console.log('user found');
                            return done(null, user.id); // user found, return that user
                        } else {
                            console.log('saving user');
                            UserSrv.saveUserBySocial('google', accessToken, profile, function(err, newUser) {

                                if (err) {
                                    throw err;
                                }

                                // if successful, return the new user
                                return done(null, newUser.id);
                            });
                        }

                    });

                } else {

                    // user already exists and is logged in, we have to link accounts
                    var user = req.user; // pull the user out of the session

                    UserSrv.saveUserBySocial('google', accessToken, profile, function(err, user) {

                        if (err) {
                            throw err;
                        }
                        console.log('update google user info .............');
                        return done(null, user.id);

                    });

                }
            });
        }
    ));



};
