'use strict';

var userValidate = require('../validation/user');
var userCtrl = require('../controller/userWebController');

var jobValidate = require('../validation/jobs');
var jobCtrl = require('../controller/jobsWebController');

//Employeer
var EmployerValidate = require('../validation/employer');
var EmployerCtrl = require('../controller/employerWebController');

var prepCtrl = require('../controller/prepWebController');

var multipart = require('connect-multiparty');
var multipartMiddleware = multipart();

module.exports = function(app,passport) {

    app.route('/api/users').post(userValidate.validateUser, userCtrl.saveUser);
    app.route('/api/users').put(userCtrl.updateUser);

    app.route('/api/currentuser/:id').get(userCtrl.findUser);

    app.route('/api/users_reg').post(userValidate.validateSignUp, userCtrl.saveUser);
    app.route('/api/users').get(userCtrl.getAllUser);

    app.route('/api/uploadpic').post(userCtrl.uploadPic);
    app.route('/api/uploadresume').post(userCtrl.uploadResume);


    app.route('/api/jobs').post(jobValidate.validateJobs, jobCtrl.saveJobs);
    app.route('/api/jobs/:_id').put(jobValidate.validateJobs, jobCtrl.updateJob)
    app.route('/api/jobs').get(jobCtrl.getAllJob);
    app.route('/api/jobsEmpbase/:_id').get(jobCtrl.getjobEmpBase);
    app.route('/jobdetails/:_jobid').get(jobCtrl.showJobDetails);

    app.route('/api/joblocation').get(prepCtrl.getAllJobLocation);
    app.route('/api/joblocation2').get(prepCtrl.getAllJobLocation2);
    app.route('/api/jobcategories').get(prepCtrl.getAllJobCategories);
    app.route('/api/skills').get(prepCtrl.getAllSkills);
    app.route('/api/findskills').post(prepCtrl.findSkills);

    app.route('/').get(function(req,res){
    	res.render('index');
    });

    app.route('/home/homepage').get(userCtrl.ensureAuthenticated,function(req, res){
        res.send(req.user);
    });

    app.get('/loggedin', function(req, res) {
      res.send(req.isAuthenticated() ? req.user : '0');
    });

    app.route('/').post(passport.authenticate('user'),function(req, res){
        console.log(req.user);
        res.send(req.user);
    })

    app.route('/Employerlogin').post(passport.authenticate('employer'),function(req, res){
        console.log(req.user);
        res.send(req.user);
    })

    app.route('/logout').post(function(req, res){
       req.logOut();
       res.status(200).end();
    });

    app.route('/home/EmployerForm').post(multipartMiddleware,EmployerValidate.validateEmployer,EmployerCtrl.createEmployer);
    app.route('/Employer').get(EmployerCtrl.readeEmployer);
    app.route('/employerDisplay/:_id').get(EmployerCtrl.readEmployerByid);
    app.route('/employerDisplay/:_id').put(EmployerValidate.validateEmployer,EmployerCtrl.updateemployer);
    app.route('/employerDisplay/:_id').delete(EmployerCtrl.deleteEmployer);
    app.route('/CompanyProfile/:_id').get(jobCtrl.getCompanyProfWithJob);


    app.route('/user').post(userValidate.validateUser, userCtrl.saveUser);
};
