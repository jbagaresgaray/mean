$(window).load(function() {

    //Pace.stop();

    // Fade out the overlay div
    $('#overlay').fadeOut(800);

    $('body').removeClass('overflow-hidden');

    //Enable animation
    $('.wrapper').removeClass('preload');
});

function blureffect(obj) {
    $(obj).on('focus', function() {
        $(obj).removeClass('scripterror');

    });
}

function parsleyLeftload() {
    var obj = '.leftchar';
    var left = parseInt($(obj).parent().find('.left').html());

    var nleft = 0;
    //console.log(typeof($(obj).val()));
    if (typeof($(obj).val()) !== 'undefined') {
        var y = $(obj).val();
        nleft = parseInt(y.length);
    }

    var r = left - nleft;
    if (r <= 0) {
        $(obj).parent().find('.left').html(0);
        return false;
    }
    $(obj).parent().find('.left').html(r);
}

function parsleyLeft(obj) {
    var left = parseInt($(obj).attr('maxlength'));
    var nleft = parseInt($(obj).val().length);
    var r = left - nleft;
    console.log(r);
    if (r <= 0) {
        $(obj).parent().find('.left').html(0);
        return false;
    }
    $(obj).parent().find('.left').html(r);

}

function checkiconNonempty() {

    $('.has-icon-alert input[type=text]').each(function() {
        if ($(this).val() != '' && $(this).val() != null)
            $(this).parent().find('.form-check').addClass('active');
        else
            $(this).parent().find('.form-check').removeClass('active');
    });

    $('.has-icon-alert input[type=email]').blur(function() {
        if ($(this).val() != '' && $(this).val() != null)
            $(this).parent().find('.form-check').addClass('active');
        else
            $(this).parent().find('.form-check').removeClass('active');
    });
    $('.has-icon-alert input[type=password]').blur(function() {
        if ($(this).val() != '' && $(this).val() != null)
            $(this).parent().find('.form-check').addClass('active');
        else
            $(this).parent().find('.form-check').removeClass('active');
    });

    $('.has-icon-alert textarea').each(function() {
        if ($(this).val() != '' && $(this).val() != null && $(this).val().length > 100)
            $(this).parent().find('.form-check').addClass('active');
        else
            $(this).parent().find('.form-check').removeClass('active');
    });

    $('.has-icon-alert select').each(function() {

        if ($(this).val() != '' && $(this).val() != null)
            $(this).parent().find('.form-check').addClass('active');
        else
            $(this).parent().find('.form-check').removeClass('active');
    });



}

function validateEmail(sEmail) {
    var filter = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,11})?$/;
    if (filter.test(sEmail)) {
        return true;
    } else {
        return false;
    }
}

function checkicon() {
    checkiconNonempty();
    //Display Check Icon
    $('.has-icon-alert input[type=text]').blur(function() {
        if ($(this).val() != '' && $(this).val() != null)
            $(this).parent().find('.form-check').addClass('active');
        else
            $(this).parent().find('.form-check').removeClass('active');
    });
    $('.has-icon-alert input[type=email]').blur(function() {
        if ($(this).val() != '' && $(this).val() != null && validateEmail($(this).val()))
            $(this).parent().find('.form-check').addClass('active');
        else
            $(this).parent().find('.form-check').removeClass('active');
    });
    $('.has-icon-alert input[type=password]').blur(function() {
        var val = $(this).val();
        if ($(this).attr('id') == 'password2' && $('#password').length == 1) {
            if (val != '' && $(this).val() != null && val == $('#password').val() && val.length > 7) {
                $(this).parent().find('.form-check').addClass('active');
            } else {
                $(this).parent().find('.form-check').removeClass('active');
            }
        } else if ($(this).attr('id') == 'password' && $('#password2').length == 1) {
            if (val != '' && val != null && val == $('#password2').val() && val.length > 7) {
                $(this).parent().find('.form-check').addClass('active');
                $('#password2').parent().find('.form-check').addClass('active');
            } else if (val != '' && val != null && val.length > 7) {
                $(this).parent().find('.form-check').addClass('active');
                $('#password2').parent().find('.form-check').removeClass('active');
            } else {
                $(this).parent().find('.form-check').removeClass('active');
                $('#password2').parent().find('.form-check').removeClass('active');
            }
        } else {
            //alert(1);
            if ($(this).val() != '' && $(this).val() != null)
                $(this).parent().find('.form-check').addClass('active');
            else
                $(this).parent().find('.form-check').removeClass('active');
        }
    });

    $('.has-icon-alert textarea').blur(function() {
        if ($(this).val() != '' && $(this).val() != null && $(this).val().length > 100)
            $(this).parent().find('.form-check').addClass('active');
        else
            $(this).parent().find('.form-check').removeClass('active');
    });

    $('.has-icon-alert select').change(function() {

        if ($(this).val() != '' && $(this).val() != null) {
            $(this).parent().find('.form-check').addClass('active');
        } else {

            $(this).parent().find('.form-check').removeClass('active');
        }
    });

    $(document).on('click', '.has-icon-alert .skill-tag', function(e) {
        $('#skillNeeded').parent().find('.form-check').addClass('active');
    });

}

function validate_signup_form() {
    console.log('validate_signup_form');

    /*if ($.trim($("#FirstName").val()) == '') {
        $("#FirstName").addClass('errorscript');
        return false;
    } else {
        $("#FirstName").removeClass('errorscript');
    }

    if ($.trim($("#LastName").val()) == '') {
        $("#LastName").addClass('scripterror');
        return false;
    } else {
        $("#LastName").removeClass('scripterror');
    }

    if ($.trim($("#email").val()) == '') {
        $("#email").addClass('scripterror');
        return false;
    } else {
        $("#email").removeClass('scripterror');
    }
    if ($("#email").val().search("@") == -1 || $("#email").val().search("[.*]") == -1) {

        $("#email").addClass("scripterror");
        return false;
    } else {
        $("#email").removeClass("scripterror");
    }
    if ($.trim($("#password").val()) == '') {
        $("#password").addClass('scripterror');
        return false;
    }
    if ($.trim($("#password").val()).length < 4) {
        $("#password").addClass('scripterror');
        return false;
    } else {
        $("#password").removeClass('scripterror');
    }
    if ($.trim($("#confirmpassword").val()) == '') {
        $("#confirmpassword").addClass('scripterror');
        return false;
    } else if ($.trim($("#confirmpassword").val()).length < 4) {
        $("#confirmpassword").addClass('scripterror');
        return false;
    } else if ($.trim($("#password").val()) != $.trim($("#confirmpassword").val())) {
        $("#confirmpassword").addClass('scripterror');
        return false;
    } else {
        $("#confirmpassword").removeClass('scripterror');
    }*/

    if (true === $('#registerform').parsley().validate('block1')) {
        // check_vendor();
        console.log('message');
    }
}

var _activeType = '';

$(document).on('click', '.signup-option-widget', function() {
    $('.signup-option-widget').removeClass('active');
    console.log('signup-option-widget');
    $(this).addClass('active');

    _activeType = $(this).attr('id');
});


$(document).on('click', '#continueSignup', function() {

    var _href = '';

    if (_activeType == 'signupType1') {
        _href = '#/EmployerForm';
    } else {
        _href = '#/join';
    }

    if (_activeType != '') {
        window.location = _href;
    }

    return false;

});
