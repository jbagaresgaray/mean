function clientaccount(obj, extra) {
    $('#overlay').fadeIn(800);
    var params = $(obj).serialize();
    console.log(params);
    $.ajax({
        type: 'POST',
        url: 'client_ajax.php',
        data: params,
        dataType: 'json',
        success: function(data) {
            $('#overlay').fadeOut(800);
            console.log(data);
            if (data.error) {
                if (data.warning != '') {
                    alert(data.warning);
                    return false;
                }
                reloadToPage(data.page);
            } else {


                if (extra == 1) {
                    $('.modal').modal('hide');
                    reloadToPage(data.page);
                } else
                    saveChanges();
                //reloadToPage(data.page);

            }
        }
    });
    /*}*/
}

function saveDefault(obj) {
    //alert(obj);

    if (true === $(obj).parsley().validate('block-card')) {
        var params = $(obj).serialize();
        $('#creditcardDefault').modal('hide');
        $('#overlay').fadeIn(800);
        $.ajax({
            type: 'POST',
            url: 'client_ajax.php',
            data: params,
            dataType: 'json',
            success: function(data) {
                $('#overlay').fadeOut(800);
                if (!data.error) {
                    $('#appendcards .btn-primary').each(function() {
                        $(this).hide();
                    });
                    var id = $('#default_card_id').val();
                    $('#append_' + id + " .btn-primary").show();
                    saveChanges();
                } else {
                    if (data.warning != '') {
                        alert(data.warning);
                        return false;
                    }

                    reloadToPage(data.page);
                }
            }
        });
    }
}

function mangecards(obj, ccid) {

    /*if(true === $(obj).parsley().validate('block1'))
    	 {	*/
    $('#overlay').fadeIn(800);
    var params = $(obj).serialize();
    console.log(params);
    $.ajax({
        type: 'POST',
        url: 'client_ajax.php',
        data: params,
        dataType: 'json',
        success: function(data) {
            $('#overlay').fadeOut(800);
            console.log(data);
            if (data.error) {
                if (data.warning != '') {
                    alert(data.warning);
                    return false;
                }
                reloadToPage(data.page);
            } else {

                $('.modal').modal('hide');
                saveChanges();
                setTimeout(function() {
                    reloadToPage(data.page);
                }, 1000);

            }
        }
    });
    /*}*/
}

function bindFileUpload() {
    var btnUpload = $('#upload-profile-photo');
    new AjaxUpload(btnUpload, {
        action: 'file_uploader.php?mode=_upload_lancer_photo',
        name: 'qqfile',
        responseType: 'json',
        onSubmit: function(file, ext) {
            if (!(ext && /^(jpg|png|jpeg|gif)$/.test(ext))) {
                // extension is not allowed
                alert('Only JPG, PNG or GIF files are allowed');
                return false;
            }
            $('#overlay').fadeIn(800);
        },
        onComplete: function(file, response) {

            if (!response.status) {
                if (response.page == '') {
                    alert(response.error);
                    return false;
                }
                reloadToPage(response.page);
                return false;
            }
            if (response.status) {
                $('.img-rounded').attr('src', response.img.replace('amp;', ''));

                //$('<li></li>').appendTo('#files').html('<img src="./uploads/'+file+'" alt="" /><br />'+file).addClass('success');
            } else {
                //$('#image_show').val('');

                alert(response.failure);
            }
            $('.img-rounded').load(function() {
                $('#overlay').fadeOut(800);
            });
        }
    });
    $('#ajax_upload_file_swer').mouseover(function() {
        $('.profile-picture-wrapper').addClass('active');
    });
    $('#ajax_upload_file_swer').mouseout(function() {
        $('.profile-picture-wrapper').removeClass('active');
    });

}

function bindFileUploadNew(photoid) {
    var btnUpload = $(photoid);
    new AjaxUpload(btnUpload, {
        action: 'file_uploader.php?mode=_upload_lancer_photo',
        name: 'qqfile',
        responseType: 'json',
        onSubmit: function(file, ext) {
            if (!(ext && /^(jpg|png|jpeg|gif)$/.test(ext))) {
                // extension is not allowed
                alert('Only JPG, PNG or GIF files are allowed');
                return false;
            }
            $('#overlay').fadeIn(800);
        },
        onComplete: function(file, response) {

            if (!response.status) {
                if (response.page == '') {
                    alert(response.error);
                    return false;
                }
                reloadToPage(response.page);
                return false;
            }
            if (response.status) {
                $('.img-rounded').attr('src', response.img.replace('amp;', ''));

                //$('<li></li>').appendTo('#files').html('<img src="./uploads/'+file+'" alt="" /><br />'+file).addClass('success');
            } else {
                //$('#image_show').val('');

                alert(response.failure);
            }
            $('.img-rounded').load(function() {
                $('#overlay').fadeOut(800);
            });
        }
    });
    $('#ajax_upload_file_swer').mouseover(function() {
        $('.profile-picture-wrapper').addClass('active');
    });
    $('#ajax_upload_file_swer').mouseout(function() {
        $('.profile-picture-wrapper').removeClass('active');
    });

}

function fileDispute() {
    $('.general-dispute').addClass('hidden');
    $('.dispute-project').fadeIn(800);
}

function submitDispute() {
    if (false === $('#dispute-form').parsley().validate('block1')) {
        return false;
    }
    $('#overlay').fadeIn(800);
    var params = $('#dispute-form').serialize();
    //console.log('client_ajax.php?'+params);
    $.ajax({
        type: 'POST',
        url: 'client_ajax.php',
        data: params,
        dataType: 'json',
        success: function(data) {
            $('#overlay').fadeOut(800);

            if (data.error) {
                if (data.warning != '') {
                    alert(data.warning);
                    return false;
                }
                reloadToPage(data.page);
            } else {
                $('#step7 .dispute-project').fadeOut(800);
                $('#step7 .dispute-confirmation').fadeIn(800);
                $("html, body").animate({
                    scrollTop: 0
                }, 600);

            }
        }
    });
}

function disputePermission() {
    $('#overlay').fadeIn(800);
    var params = {
        cmd: '_dispute_assistance'
    };
    $.ajax({
        type: 'POST',
        url: 'client_ajax.php',
        data: params,
        dataType: 'json',
        success: function(data) {
            $('#overlay').fadeOut(800);
            console.log(data);
            if (data.error) {
                if (data.warning != '') {
                    alert(data.warning);
                    return false;
                }
                reloadToPage(data.page);
            } else {
                $('#step7').html(data.html);

            }
        }
    });
}

function account_steps(id) {
    //$('#overlay').fadeIn(800);
    $('.sidebar-menu ul li').removeClass('active');
    $('.' + id).addClass('active');
    $('#step1').hide();
    $('#step2').hide();
    $('#step3').hide();
    $('#step4').hide();
    $('#step5').hide();
    $('#step6').hide();
    $('#step7').hide();

    //alert(id);

    if (id == 'step1')
        Confirm('#accountdetails');
    else if (id == 'step2')
        Confirm('#clinet-account-edit');
    else if (id == 'step3')
        Confirm('#passwordchange');
    else if (id == 'step4')
        Confirm('#notifications');
    else if (id == 'step5')
        Confirm('');

    if (id == 'step6') {
        paymentData(0);
        $('#step6').show();

        FormName = false;

        $('.save-changes-header').hide();
    }
    //$('#overlay').fadeOut(800);
    else {
        $('#' + id).show();
        $('.save-changes-header').hide();

        $("html, body").animate({
            scrollTop: 0
        }, 600);
        return false;
    }
}


function isnumeric(tis) {
    tis.value = tis.value.replace(/[^0-9\.]/g, '');
    return true;
}


function fucusnext(tis) {
    //alert(1);
    if ($(tis).val().length > 2) {
        //alert('dss');
        $(tis).next('span').next('input').focus();
    }
}
