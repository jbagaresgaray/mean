'use strict';

var SessionFactory = function (CREDENTIALS, $http) {
	var SessionFactory = {};
	var session = null;
	SessionFactory.subscriber = 0;

	SessionFactory.getCredentials = function (name, cb){
		$http.get('/api/v1/session/' + name)
			.success(function (result, status, headers, config){
				CREDENTIALS.SESSION_ID = result.data.game_info.sessionId;
				CREDENTIALS.TOKEN = result.data.token;
				session = OT.initSession(CREDENTIALS.API_KEY, CREDENTIALS.SESSION_ID);
				return cb(true);
			})
			.error(function (data, status, headers, config) {
				throw new Error('Error fetching');
			});
	};

	SessionFactory.connect = function () {
		session.connect(CREDENTIALS.TOKEN, function (err) {
			if(err instanceof Error){
				throw new Error('Error connecting: ', err);
			}
		});
	};

	SessionFactory.connectAndPublish = function(elementId) {
		session.connect(CREDENTIALS.TOKEN, function (err) {
			if(err instanceof Error){
				throw new Error('Error connecting: ', err);
			} else {
				var options = {
					width: 120,
					height: 120,
					name: "Guest"
				};
				var publisher = OT.initPublisher(elementId, options);
				session.publish(publisher);
				SessionFactory.subscriber++;
			}
		});
	};

	SessionFactory.publish = function (elementId) {
		var options = {
			width: 120,
			height: 120,
			name: "Guest-" + Date.now()
		};
		var publisher = OT.initPublisher(elementId, options);
		session.publish(publisher);
		SessionFactory.subsribers++;
	};

	SessionFactory.subscribe = function (elementId) {
		session.on('streamCreated', function (event){
			session.subscribe(event.stream);
			SessionFactory.subscriber++;
		});
	};

	return SessionFactory;
};

Application.Services.factory('SessionFactory', ['CREDENTIALS', '$http', SessionFactory]);
