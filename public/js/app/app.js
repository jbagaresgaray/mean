'use strict';


var Application = Application || {};

Application.Controllers = angular.module('app.controllers', []);
Application.Services = angular.module('app.services', []);
Application.Constants = angular.module('app.constants', []);
Application.Directives = angular.module('app.directives', []);
Application.Values = angular.module('app.values', []);

var appDeps = ['app.controllers', 'app.services', 'app.constants', 'app.directives', 'app.values'];
angular.module('application', appDeps);
