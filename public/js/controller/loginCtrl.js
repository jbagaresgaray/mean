'use strict';

var loginCtrl = function($scope, $location, UserFactory) {
    $scope.user = {};
    $scope.showme = true;
    $scope.submitForm = function(valid) {
        if (true === $('#login').parsley().validate('block1')) {
            console.log('message submitForm');
            UserFactory.userLogin($scope.user, function(data) {
                localStorage.setItem('Person', 'user');
                $location.url('/homepage');
                window.location.reload();
            });
        }
    }

    if ($location.path() == '/Employerlogin') {
        $scope.showme = true;
    } else {
        $scope.showme = false;
    }
}


var EmpLoginCtrl = function($scope, $location, UserFactory) {
    $scope.user = {};
    $scope.submitForm = function(valid) {
        if (true === $('#loginForm').parsley().validate('block1')) {
            UserFactory.EmpuserLogin($scope.user, function(data) {
                localStorage.setItem('Person', 'employer');
                $location.url('/homepage');
                window.location.reload();
            });
        }
    }
}


mindanaoJobs.Controllers.controller('loginCtrl', ['$scope', '$location', 'UserFactory', loginCtrl]);
mindanaoJobs.Controllers.controller('EmpLoginCtrl', ['$scope', '$location', 'UserFactory', EmpLoginCtrl]);
