'use strict';
(function() {

    var showErrors = function() {
        return {
            restrict: 'A',
            require: '^form',
            link: function(scope, el, attrs, formCtrl) {

                var inputEl = el[0].querySelector("[name]");
                var inputNgEl = angular.element(inputEl);
                var inputName = inputNgEl.attr('name');

                var blurred = false;
                inputNgEl.bind('blur', function() {
                    el.toggleClass('has-error', formCtrl[inputName].$invalid);
                });

                scope.$watch(function() {
                    return scope.showErrorsCheckValidity;
                }, function(newVal, oldVal) {
                    if (!newVal) {
                        return;
                    }
                    el.toggleClass('has-error', formCtrl[inputName].$invalid);
                });


                scope.$on('show-errors-check-validity', function() {
                    el.toggleClass('has-error', formCtrl[inputName].$invalid);
                });
            }
        }
    }

    mindanaoJobs.Directive.directive('showErrors', [showErrors]);

}());
