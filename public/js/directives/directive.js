'use strict';
(function(){

	var piSidebar = function(){
		return{
			restrict: 'AE',
			replace: true,
			templateUrl: 'public/views/sidebar.html'
		}
	}

	var piHeader = function(){
		return{
			restrict: 'AE',
			replace: true,
			templateUrl: 'public/views/header-profile.html',
			controller: 'mainCtrl'
		}
	}

	var piFooter = function(){
		return{
			restrict: 'AE',
			replace: true,
			templateUrl: 'public/views/footer.html',
		}
	}

	var piHeaderlogin = function(){
		return{
			restrict: 'AE',
			replace: true,
			templateUrl: 'public/views/header-login.html',
			controller : 'loginCtrl'
		}
	}

	var fileread = function(){
		return {
	        scope: {
	            fileread: "="
	        },
	        link: function (scope, element, attributes) {
	            element.bind("change", function (changeEvent) {
	                scope.$apply(function () {
	                    scope.fileread = changeEvent.target.files[0];
	                });
	            });
	        }
	    }
	}

	mindanaoJobs.Directive.directive('piSidebar',[piSidebar]);
	mindanaoJobs.Directive.directive('piHeader',[piHeader]);
	mindanaoJobs.Directive.directive('piFooter',[piFooter]);
	mindanaoJobs.Directive.directive('piHeaderlogin',[piHeaderlogin]);
	mindanaoJobs.Directive.directive('fileread',[fileread]);

}());
