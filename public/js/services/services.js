'use strict';

(function() {

    var Defaultfactory = function($http) {
        var Defaultfactory = {};

        Defaultfactory.getAllJobLocation = function(cb) {
            $http.get('/api/joblocation').success(function(data) {
                cb(data);
            });
        };

        Defaultfactory.getAllJobLocation2 = function(cb) {
            $http.get('/api/joblocation2').success(function(data) {
                cb(data);
            });
        };

        Defaultfactory.getAllJobCategories = function(cb) {
            $http.get('/api/jobcategories').success(function(data) {
                cb(data);
            });
        };

        Defaultfactory.getAllSkills = function(cb) {
            $http.get('/api/skills').success(function(data) {
                cb(data);
            });
        };

        Defaultfactory.findSkills = function(param,cb) {
            $http.post('/api/findskills',param).success(function(data) {
                cb(data);
            });
        };

        return Defaultfactory;
    };

    mindanaoJobs.Services.factory('Defaultfactory', ['$http', Defaultfactory]);

}());
