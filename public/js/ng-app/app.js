'use strict';

var mindanaoJobs = mindanaoJobs || {};

mindanaoJobs.Controllers = angular.module('minJob.controllers', []);
mindanaoJobs.Services = angular.module('minJob.services', []);
mindanaoJobs.Directive = angular.module('minJob.directives', []);
mindanaoJobs.Filters = angular.module('minJob.filters', []);

(function() {
    var config = function($routeProvider, $httpProvider) {

            //Check if the user is connected
            var checkLoggedin = function($q, $timeout, $http, $location, $rootScope) {
                // Initialize a new promise
                var deferred = $q.defer();

                // Make an AJAX call to check if the user is logged in
                $http.get('/loggedin').success(function(user) {
                    // Authenticated
                    if (user !== '0')
                        $timeout(deferred.resolve, 0);
                    // Not Authenticated
                    else {
                        $rootScope.message = 'You need to log in.';
                        $timeout(function() {
                            deferred.reject();
                        }, 0);
                        $location.url('/login');
                    }
                });
                return deferred.promise;
            };

            // Add an interceptor for AJAX errors

            /*$httpProvider.responseInterceptors.push(function($q, $location) {
                return function(promise) {
                    return promise.then(
                        // Success: just return the response
                        function(response) {
                            return response;
                        },
                        // Error: check the error status to get only the 401
                        function(response) {
                            if (response.status === 401)
                                $location.url('/login');
                            return $q.reject(response);
                        }
                    );
                }
            });*/

            $httpProvider.interceptors.push(function($q, $location) {
                return function(promise) {
                    return promise.then(
                        // Success: just return the response
                        function(response) {
                            return response;
                        },
                        // Error: check the error status to get only the 401
                        function(response) {
                            if (response.status === 401)
                                $location.url('/login');
                            return $q.reject(response);
                        }
                    );
                }
            });

            $routeProvider
                .when('/', {
                    templateUrl: '/public/views/home-main.html'
                })
                .when('/login', {
                    templateUrl: '/public/views/login.html',
                    controller: 'loginCtrl'
                })
                .when('/homepage', {
                    templateUrl: '/public/views/home-main.html',
                    resolve: {
                        loggedin: checkLoggedin
                    }
                })
                .when('/profile', {
                    templateUrl: '/public/views/my-profile.html',
                    controller: 'userCtrl'
                })
                .when('/my-profile', {
                    templateUrl: 'public/views/view.html',
                    controller: 'userCtrl'
                })
                .when('/employer', {
                    templateUrl: '/public/views/employer-login.html',
                    controller: 'EmpLoginCtrl'
                })
                .when('/EmployerForm', {
                    templateUrl: '/public/views/employerForm.html',
                    controller: 'employerCtrl'
                })
                .when('/EmployerProfile', {
                    templateUrl: '/public/views/employerprofile.html',
                    controller: 'employerCtrl'
                })
                .when('/CompanyProfile/:_id',{
                    templateUrl: '/public/views/companyprofile.html',
                    controller: 'companyprofileCtrl'
                })
                .when('/EmployerAccount',{
                    templateUrl: '/public/views/employerAccountInfo.html',
                    controller: 'employerCtrl'
                })
                .when('/MyJobs',{
                    templateUrl: 'public/views/employerJobList.html',
                    controller: 'employerCtrl'
                })
                .when('/joblist', {
                    templateUrl: '/public/views/joblist.html',
                    controller: 'jobCtrl'
                })
                .when('/account', {
                    templateUrl: '/public/views/account.html',
                    controller: 'userCtrl'
                })
                .when('/jobdetails/:_id', {
                    templateUrl: '/public/views/job-detail2.html',
                    controller: 'jobDetailsCtrl'
                })
                .when('/Categories', {
                    templateUrl: '/public/views/categories.html',
                    controller: 'jobCtrl'
                })
                .when('/join', {
                    templateUrl: 'public/views/join.html',
                    controller: 'signUpCtrl'
                })
                .when('/privacy', {
                    templateUrl: 'public/views/privacy.html'
                })
                .when('/terms', {
                    templateUrl: 'public/views/terms.html'
                })
                .when('/joinoption', {
                    templateUrl: 'public/views/joinoption.html',
                    controller: 'signUpCtrl'
                })
                .when('/jobpost', {
                    templateUrl: 'public/views/job-post.html',
                    controller: 'jobCtrl'
                })
                .when('/jobpost/:_id', {
                    templateUrl: 'public/views/job-post.html',
                    controller: 'updateJobCtrl'
                })
                .when('/aboutus', {
                    templateUrl: 'public/views/aboutus.html'
                })
                .when('/recovery', {
                    templateUrl: 'public/views/recovery.html',
                    controller: 'recoverCtrl'
                })
                .when('/messages', {
                    templateUrl: 'public/views/messages.html',
                })
                .when('/notifications', {
                    templateUrl: 'public/views/notifications.html',
                })
                .otherwise({
                    redirectTo: '/'
                });
        } // end of config

    angular
        .module('minJob', ['cgNotify', 'ui.bootstrap', 'ui.select', 'ngRoute', 'ngResource', 'ngAnimate', 'ngSanitize','angularFileUpload', 'minJob.controllers', 'minJob.services', 'minJob.directives','minJob.filters'])

        .config(config)
        .run(function($rootScope, $http) {
            $rootScope.message = '';

            // Logout function is available in any pages
            $rootScope.logout = function() {
                $rootScope.message = 'Logged out.';
                $http.post('/logout');
                window.localStorage.clear();
                window.location.reload();
            };
        })
        .filter('propsFilter', function() {
            return function(items, props) {
                var out = [];

                if (angular.isArray(items)) {
                    items.forEach(function(item) {
                        var itemMatches = false;

                        var keys = Object.keys(props);
                        for (var i = 0; i < keys.length; i++) {
                            var prop = keys[i];
                            var text = props[prop].toLowerCase();
                            if (item[prop].toString().toLowerCase().indexOf(text) !== -1) {
                                itemMatches = true;
                                break;
                            }
                        }

                        if (itemMatches) {
                            out.push(item);
                        }
                    });
                } else {
                    // Let the output be the input untouched
                    out = items;
                }

                return out;
            }
        });
}());
