'use strict';

module.exports = {
    dbUrl: process.env.DB_URL || 'mongodb://localhost:27017/mindanaojobs',
    db_user: process.env.DB_USER || 'mindanaojobs',
    db_password: process.env.DB_USER || '12345',
    port: process.env.APP_PORT || 3000,
    socket_port: process.env.SOCKET_PORT || 3333,
    app_name: process.env.APP_NAME || "Mindanaojobs",
    api_host_url: process.env.API_HOST_URL || 'http://localhost:3000',
    api_version: '/api/v1'
};
